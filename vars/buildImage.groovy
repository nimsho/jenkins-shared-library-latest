#!/usr/bin/env groovy

def call(){
    echo "building the docker image.."
    withCredentials([usernamePassword(credentialsId: "docker-hub-credential", usernameVariable: "USER", passwordVariable: "PASS")]) {
    sh "docker build -t nshonubi/demo-app:jma-2.2 ."
    sh "echo $PASS | docker login -u $USER --password-stdin"
    sh "docker push nshonubi/demo-app:jma-2.2"
    }
}